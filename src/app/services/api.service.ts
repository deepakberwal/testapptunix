import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  getAllProducts: any;

  // data :any = new BehaviorSubject('')

  constructor(private http:HttpClient) {} 
  postProduct(data:any){
  return this.http.post("https://dummyapi.io/data/v1/user/create",data,this.header())
  }
  getProduct(url:any){
    return this.http.get("https://dummyapi.io/data/v1/"+url,this.header())
  }
  getProductById(url:any,id:any){
    return this.http.get("https://dummyapi.io/data/v1/"+url+'/'+id,this.header())
  }
  putProduct(url:any,data:any,id:number){
return this.http.put("https://dummyapi.io/data/v1/"+url+'/'+id,data,this.header())
  }
  deleteProduct(id:number){
    return this.http.delete("https://dummyapi.io/data/v1/user/"+id,this.header())
  }
  header() {
      const headers = new HttpHeaders({
        "cache-control": "no-cache",
        "content-type": "application/json",
        "app-id": '6332065aa4ebb5ba550602f5'
      });
      const option = {
        headers,
      };
      return option;
  }
}

 