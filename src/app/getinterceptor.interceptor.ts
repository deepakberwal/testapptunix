

import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GetinterceptorInterceptor {

  constructor(
    private router: Router,
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(tap((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
      }
    }, (error: any) => {
      if (error['status'] === 401) {
        localStorage.clear();
        this.router.navigate(['auth']);
      }
      else if(error['status'] === 403){
        alert('Working');
      } 
      else if(error['status'] === 408){
        console.log("208 intercept");
      }
      else if(error['status'] === 407){
      }
      else if(error['status'] === 400){
      }
      else if(error['status'] === 500){
      }
      else if(error['status'] === 404){
      }else{
      }
    }));
  }

}

