import { TestBed } from '@angular/core/testing';

import { GetinterceptorInterceptor } from './getinterceptor.interceptor';

describe('GetinterceptorInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      GetinterceptorInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: GetinterceptorInterceptor = TestBed.inject(GetinterceptorInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
