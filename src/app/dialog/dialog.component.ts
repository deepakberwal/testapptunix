import { Component, OnInit,Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../services/api.service';
import { MatDialogRef,MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {
productForm!:FormGroup;
data:any;
actionbtn:string="save"
  constructor(private fb:FormBuilder,
             @Inject(MAT_DIALOG_DATA) public editData:any,   
             private api:ApiService,
             private dialogRef:MatDialogRef<DialogComponent>
    ) { }

  ngOnInit(): void {
    this.productForm=this.fb.group({
      firstName:['',[Validators.required]],
      lastName:['',[Validators.required]],
      email:['',[Validators.required,Validators.email]],
    })
    
    if(this.editData){
      console.log(this.editData);
      this.api.getProductById('user',this.editData.id).subscribe({
        next:(res:any)=>{
          this.data = res;
          this.productForm.controls['firstName'].setValue(this.data.firstName);
          this.productForm.controls['lastName'].setValue(this.data.lastName);
          this.productForm.controls['email'].setValue(this.data.email);
        },
        })
      this.actionbtn="update";
    }
  
  }
addProduct(){
if(this.productForm.valid){
  this.api.postProduct(this.productForm.value).subscribe({
    next:(res)=>{
      alert("added succesfully")
      this.productForm.reset();
      this.dialogRef.close('save')
    },
    error:()=>{
      alert("Error while adding ")
    }
  })
}
  
}
  getAllProducts() {
    throw new Error('Method not implemented.');
  }
updateproduct(){
  this.api.putProduct('user ',this.productForm.value,this.editData.id).subscribe({
    next:(res)=>{
      alert("user Updated");
      this.productForm.reset();
      this.dialogRef.close('update');
    this.api.getAllProducts();
    } 
  })
}


}
