import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialog,MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogComponent } from './dialog/dialog.component';
import { ApiService } from './services/api.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'crud13';
  length:any;
data: any;
recordsLength =0;
  pageSize = 20;
currentPage = 0;
displayedColumns: string[] = ['id', 'name', 'phone', 'email','action'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  constructor(private dialog:MatDialog,
              public api:ApiService    
    ){}
  ngOnInit(): void {
   this.getAllProducts()
  }

  openDialog(){
this.dialog.open(DialogComponent,{
width:'30%'
});
  }
  getAllProducts(){
    this.api.getProduct('user').subscribe({
      next:(res:any)=>{
        
        this.dataSource= new MatTableDataSource(res.data);
        this.recordsLength =res.total;
      },
      error:(err)=>{
        alert("error")
      }
      });
  }
  editProduct(row:any){
    this.dialog.open(DialogComponent,{
      width:'30%',
      data:row
    })
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  deleteProduct(id:number){
this.api.deleteProduct(id).subscribe({
  
  next:(res=>{
    this.getAllProducts();
    alert("Deleted Succesfully")
  })
})
  }
}
